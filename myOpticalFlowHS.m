%myOpticalFlowHS geschrieben von Nyikos Peter
%myOpticalFlowHS Estimate optical flow using Horn-Schunck algorithm.
classdef myOpticalFlowHS < handle
    
  properties(SetAccess=public)
    Smoothness = 1;
    MaxIteration = int32(10);
    VelocityDifference = 0;    
  end
  
  properties(Hidden, Access=private)
    pPreviousFrameBuffer;
    pFirstCall;
   
  end
  
  methods

    % Constructor
    function obj = myOpticalFlowHS(smooth,iter,veloc)

      obj.Smoothness = smooth;
      obj.MaxIteration = iter;
      obj.VelocityDifference = veloc;
      obj.pFirstCall = true;
      
    end
    

    % Predict method
    function outFlow = estimateFlow(obj, ImageA)

     checkImage(ImageA);
     
     if isa(ImageA, 'double') 
         tmpImageA = ImageA;
     else
         if isa(ImageA, 'uint8') 
             tmpImageA = ImageA;
         else
             tmpImageA = im2single(ImageA);
         end
     end    
     
     if (obj.pFirstCall) 
        obj.pPreviousFrameBuffer = zeros(size(tmpImageA), 'like', tmpImageA);      
     end
     ImageB = obj.pPreviousFrameBuffer;          
     
      [outVelReal, outVelImag] = HS(tmpImageA,ImageB,obj.Smoothness,obj.MaxIteration);
      
      if(obj.pFirstCall)
          
        obj.pFirstCall = false;
        outVelReal = zeros;
        outVelImag = zeros;
      
      end
     tempOutFlow = myopticalFlow(outVelReal, outVelImag);
     
     A = tempOutFlow.Vx;
     B = tempOutFlow.Vy;
     %C = tempOutFlow.Magnitude;
     
     %A(C < 10) = 0;
     %B(C < 10) = 0;
     
     outFlow = myopticalFlow(A,B);
     
     obj.pPreviousFrameBuffer = tmpImageA;
    end

    function set.Smoothness(this, smoothness)
        
        this.Smoothness = double(smoothness);
    end

    function set.MaxIteration(this, maxIteration)
        
        this.MaxIteration = int32(maxIteration);
    end
    
    function set.VelocityDifference(this, velocityDifference)
        
        this.VelocityDifference = double(velocityDifference);
    end    
      
  end

end
              
function checkImage(I)

validateattributes(I,{'uint8', 'int16', 'double', 'single', 'logical'}, ...
    {'real','nonsparse', '2d'}, mfilename, 'ImageA', 1)

end