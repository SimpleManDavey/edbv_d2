# EDBV_D2

Ausführung des Frameworks:

Man öffnet die App, indem man die .mlapp Datei öffnet und ausführt.
Mit den Buttons links oben kann man entweder ein einzelnes Video oder einen ganzen Ordner zur Analyse auswählen. Dabei öffnet sich ein FileBrowser zur Auswahl des Videos bzw. Ordners.
Das Video soll ein Buchen- oder Ahornblatt zeigen, dass vor einem einfarbigen Hintergrund zu Boden fällt.
Nachdem ein Video od. Ordner ausgewählt wurde, beginnt das Programm sofort mit der Analyse. Weitere Einstellungen sind nicht notwendig.
Der Abbruch-Knopf stoppt die gesamte Analyse und gibt ein frühzeitiges Ergebnis für das aktuelle Video.

Die diversen Plots und Bilder dienen als optischen Feedback über den Arbeitsfortschritt des Programms:
- Das Blatt-Stempel-Bild zeigt den erkannten optischen Fluss jedes Frames als gesammeltes Stempelbild.
- Die Vektor-Summen-Diagramme zeigen die errechnete Summe der erkannten Vektoren pro Richtung.
- In den 'Live-Plots' sieht man 3 verschiedene Bildausgaben:
-- das aktuelle Frame des Videos
-- das Vektorbild des Frames
-- das kombinierte Bild

Über dem Stempel-Bild ist die Fortschritt-Anzeige. Nach Start der Analyse ist dort die Anzahl der bereits analysierten Frames sichtbar.
Sobald die Analyse fertig ist, steht dort auch das Ergebnis.
Zusätzlich wird das Ergebnis nach Abschluss der Analyse in der Eregnis-Box unterhalb des Stempel-Bildes dargestellt

Im Panel 'Program Settings' gibt es die Möglichkeit Programmeinstellungen zu verändern.
- Mit der 'Pause Time' kann eingestellt werden, wie lange das Programm zwischen der Analyse von 2 Frames pausiert.
- Bei aktivierter Screenshot-Checkbox speichert das Programm nach Ende der Analyse einen Screenshot des Programmes in den 'Result'-Unterordner. Der Name des Videos wird als Name des Bildes übernommen.
	Diese Funktion sollte in Kombination mit der Ordner-Analyse verwendet werden, da so die Ergebnisse nach Abschluss der Analyse angenehmt betrachtet werden können.
- Der 'Fast Mode' deaktiviert das Zeichnen der 'Live-Plots' und sorgt so für eine starke Performance-Verbesserung.