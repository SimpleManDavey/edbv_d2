%HS geschrieben von Nyikos Peter
function [u, v] = HS(im1, im2, alpha, ite)

im1 = double(im1);
im2 = double(im2);

im1 = gaussSmooth(im1,5);
im2 = gaussSmooth(im2,5);

u = 0;
v = 0;

[fx, fy, ft] = computeDerivatives(im1, im2);

kernel_1=[1/12 1/6 1/12;1/6 0 1/6;1/12 1/6 1/12];

% Iterations
for i=1:ite
    uAvg=conv2(u,kernel_1,'same');
    vAvg=conv2(v,kernel_1,'same');

    u = uAvg - ( fx .* ( ( fx .* uAvg ) + ( fy .* vAvg ) + ft ) ) ./ ( alpha^2 + fx.^2 + fy.^2); 
    v = vAvg - ( fy .* ( ( fx .* uAvg ) + ( fy .* vAvg ) + ft ) ) ./ ( alpha^2 + fx.^2 + fy.^2);
end

u(isnan(u)) = 0;
v(isnan(v)) = 0;

end

function result = gaussSmooth(img,value)

G = gaussFilter(value);
result = conv2(img,G,'same');
result = conv2(result,G','same');

end

function G = gaussFilter(value)

kSize=2*(value*3);
x=-(kSize/2):(1+1/kSize):(kSize/2);
G=(1/(sqrt(2*pi)*value)) * exp (-(x.^2)/(2*value^2));

end

function [fx, fy, ft] = computeDerivatives(im1, im2)

if (size(im2,1) == 0)
    im2 = zeros(size(im1));
end

fx = conv2(im1,0.25* [-1 1; -1 1],'same') + conv2(im2, 0.25*[-1 1; -1 1],'same');
fy = conv2(im1, 0.25*[-1 -1; 1 1], 'same') + conv2(im2, 0.25*[-1 -1; 1 1], 'same');
ft = conv2(im1, 0.25*ones(2),'same') + conv2(im2, -0.25*ones(2),'same');
end